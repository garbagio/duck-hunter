// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FVector;
struct FHitResult;
#ifdef DUCKHUNTER_DuckHunterProjectile_generated_h
#error "DuckHunterProjectile.generated.h already included, missing '#pragma once' in DuckHunterProjectile.h"
#endif
#define DUCKHUNTER_DuckHunterProjectile_generated_h

#define duck_hunter_Source_DuckHunter_DuckHunterProjectile_h_12_SPARSE_DATA
#define duck_hunter_Source_DuckHunter_DuckHunterProjectile_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnHit) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_HitComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_STRUCT(FVector,Z_Param_NormalImpulse); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnHit(Z_Param_HitComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_NormalImpulse,Z_Param_Out_Hit); \
		P_NATIVE_END; \
	}


#define duck_hunter_Source_DuckHunter_DuckHunterProjectile_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnHit) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_HitComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_STRUCT(FVector,Z_Param_NormalImpulse); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnHit(Z_Param_HitComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_NormalImpulse,Z_Param_Out_Hit); \
		P_NATIVE_END; \
	}


#define duck_hunter_Source_DuckHunter_DuckHunterProjectile_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesADuckHunterProjectile(); \
	friend struct Z_Construct_UClass_ADuckHunterProjectile_Statics; \
public: \
	DECLARE_CLASS(ADuckHunterProjectile, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DuckHunter"), NO_API) \
	DECLARE_SERIALIZER(ADuckHunterProjectile) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define duck_hunter_Source_DuckHunter_DuckHunterProjectile_h_12_INCLASS \
private: \
	static void StaticRegisterNativesADuckHunterProjectile(); \
	friend struct Z_Construct_UClass_ADuckHunterProjectile_Statics; \
public: \
	DECLARE_CLASS(ADuckHunterProjectile, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DuckHunter"), NO_API) \
	DECLARE_SERIALIZER(ADuckHunterProjectile) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define duck_hunter_Source_DuckHunter_DuckHunterProjectile_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ADuckHunterProjectile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ADuckHunterProjectile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ADuckHunterProjectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADuckHunterProjectile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ADuckHunterProjectile(ADuckHunterProjectile&&); \
	NO_API ADuckHunterProjectile(const ADuckHunterProjectile&); \
public:


#define duck_hunter_Source_DuckHunter_DuckHunterProjectile_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ADuckHunterProjectile(ADuckHunterProjectile&&); \
	NO_API ADuckHunterProjectile(const ADuckHunterProjectile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ADuckHunterProjectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADuckHunterProjectile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ADuckHunterProjectile)


#define duck_hunter_Source_DuckHunter_DuckHunterProjectile_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CollisionComp() { return STRUCT_OFFSET(ADuckHunterProjectile, CollisionComp); } \
	FORCEINLINE static uint32 __PPO__ProjectileMovement() { return STRUCT_OFFSET(ADuckHunterProjectile, ProjectileMovement); }


#define duck_hunter_Source_DuckHunter_DuckHunterProjectile_h_9_PROLOG
#define duck_hunter_Source_DuckHunter_DuckHunterProjectile_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	duck_hunter_Source_DuckHunter_DuckHunterProjectile_h_12_PRIVATE_PROPERTY_OFFSET \
	duck_hunter_Source_DuckHunter_DuckHunterProjectile_h_12_SPARSE_DATA \
	duck_hunter_Source_DuckHunter_DuckHunterProjectile_h_12_RPC_WRAPPERS \
	duck_hunter_Source_DuckHunter_DuckHunterProjectile_h_12_INCLASS \
	duck_hunter_Source_DuckHunter_DuckHunterProjectile_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define duck_hunter_Source_DuckHunter_DuckHunterProjectile_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	duck_hunter_Source_DuckHunter_DuckHunterProjectile_h_12_PRIVATE_PROPERTY_OFFSET \
	duck_hunter_Source_DuckHunter_DuckHunterProjectile_h_12_SPARSE_DATA \
	duck_hunter_Source_DuckHunter_DuckHunterProjectile_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	duck_hunter_Source_DuckHunter_DuckHunterProjectile_h_12_INCLASS_NO_PURE_DECLS \
	duck_hunter_Source_DuckHunter_DuckHunterProjectile_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DUCKHUNTER_API UClass* StaticClass<class ADuckHunterProjectile>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID duck_hunter_Source_DuckHunter_DuckHunterProjectile_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
