// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DuckHunter/DuckHunterGameMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDuckHunterGameMode() {}
// Cross Module References
	DUCKHUNTER_API UClass* Z_Construct_UClass_ADuckHunterGameMode_NoRegister();
	DUCKHUNTER_API UClass* Z_Construct_UClass_ADuckHunterGameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_DuckHunter();
// End Cross Module References
	void ADuckHunterGameMode::StaticRegisterNativesADuckHunterGameMode()
	{
	}
	UClass* Z_Construct_UClass_ADuckHunterGameMode_NoRegister()
	{
		return ADuckHunterGameMode::StaticClass();
	}
	struct Z_Construct_UClass_ADuckHunterGameMode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ADuckHunterGameMode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_DuckHunter,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADuckHunterGameMode_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "DuckHunterGameMode.h" },
		{ "ModuleRelativePath", "DuckHunterGameMode.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ADuckHunterGameMode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ADuckHunterGameMode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ADuckHunterGameMode_Statics::ClassParams = {
		&ADuckHunterGameMode::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008802ACu,
		METADATA_PARAMS(Z_Construct_UClass_ADuckHunterGameMode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ADuckHunterGameMode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ADuckHunterGameMode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ADuckHunterGameMode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ADuckHunterGameMode, 3647894175);
	template<> DUCKHUNTER_API UClass* StaticClass<ADuckHunterGameMode>()
	{
		return ADuckHunterGameMode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ADuckHunterGameMode(Z_Construct_UClass_ADuckHunterGameMode, &ADuckHunterGameMode::StaticClass, TEXT("/Script/DuckHunter"), TEXT("ADuckHunterGameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ADuckHunterGameMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
