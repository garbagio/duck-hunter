// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DUCKHUNTER_DuckHunterHUD_generated_h
#error "DuckHunterHUD.generated.h already included, missing '#pragma once' in DuckHunterHUD.h"
#endif
#define DUCKHUNTER_DuckHunterHUD_generated_h

#define duck_hunter_Source_DuckHunter_DuckHunterHUD_h_12_SPARSE_DATA
#define duck_hunter_Source_DuckHunter_DuckHunterHUD_h_12_RPC_WRAPPERS
#define duck_hunter_Source_DuckHunter_DuckHunterHUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define duck_hunter_Source_DuckHunter_DuckHunterHUD_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesADuckHunterHUD(); \
	friend struct Z_Construct_UClass_ADuckHunterHUD_Statics; \
public: \
	DECLARE_CLASS(ADuckHunterHUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/DuckHunter"), NO_API) \
	DECLARE_SERIALIZER(ADuckHunterHUD)


#define duck_hunter_Source_DuckHunter_DuckHunterHUD_h_12_INCLASS \
private: \
	static void StaticRegisterNativesADuckHunterHUD(); \
	friend struct Z_Construct_UClass_ADuckHunterHUD_Statics; \
public: \
	DECLARE_CLASS(ADuckHunterHUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/DuckHunter"), NO_API) \
	DECLARE_SERIALIZER(ADuckHunterHUD)


#define duck_hunter_Source_DuckHunter_DuckHunterHUD_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ADuckHunterHUD(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ADuckHunterHUD) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ADuckHunterHUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADuckHunterHUD); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ADuckHunterHUD(ADuckHunterHUD&&); \
	NO_API ADuckHunterHUD(const ADuckHunterHUD&); \
public:


#define duck_hunter_Source_DuckHunter_DuckHunterHUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ADuckHunterHUD(ADuckHunterHUD&&); \
	NO_API ADuckHunterHUD(const ADuckHunterHUD&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ADuckHunterHUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADuckHunterHUD); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ADuckHunterHUD)


#define duck_hunter_Source_DuckHunter_DuckHunterHUD_h_12_PRIVATE_PROPERTY_OFFSET
#define duck_hunter_Source_DuckHunter_DuckHunterHUD_h_9_PROLOG
#define duck_hunter_Source_DuckHunter_DuckHunterHUD_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	duck_hunter_Source_DuckHunter_DuckHunterHUD_h_12_PRIVATE_PROPERTY_OFFSET \
	duck_hunter_Source_DuckHunter_DuckHunterHUD_h_12_SPARSE_DATA \
	duck_hunter_Source_DuckHunter_DuckHunterHUD_h_12_RPC_WRAPPERS \
	duck_hunter_Source_DuckHunter_DuckHunterHUD_h_12_INCLASS \
	duck_hunter_Source_DuckHunter_DuckHunterHUD_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define duck_hunter_Source_DuckHunter_DuckHunterHUD_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	duck_hunter_Source_DuckHunter_DuckHunterHUD_h_12_PRIVATE_PROPERTY_OFFSET \
	duck_hunter_Source_DuckHunter_DuckHunterHUD_h_12_SPARSE_DATA \
	duck_hunter_Source_DuckHunter_DuckHunterHUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	duck_hunter_Source_DuckHunter_DuckHunterHUD_h_12_INCLASS_NO_PURE_DECLS \
	duck_hunter_Source_DuckHunter_DuckHunterHUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DUCKHUNTER_API UClass* StaticClass<class ADuckHunterHUD>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID duck_hunter_Source_DuckHunter_DuckHunterHUD_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
